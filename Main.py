

# Crea instancias de diferentes animales
from Leon import Leon
from Mono import Mono
from Tigre import Tigre

def registrar_animal(animal):
    print(f"Nombre: {animal.nombre}")
    print(f"Especie: {animal.especie}")
    print(f"Sonido: {animal.hacer_sonido()}")
    print()

leon = Leon("Simba", "León")
tigre = Tigre("Tony", "Tigre")
mono = Mono("Kiara", "Mono capuchino")

# Registra los animales en el zoológico
registrar_animal(leon)
registrar_animal(tigre)
registrar_animal(mono)


